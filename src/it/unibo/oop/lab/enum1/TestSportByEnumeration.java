package it.unibo.oop.lab.enum1;

import java.util.ArrayList;
import java.util.List;

import it.unibo.oop.lab.socialnetwork.User;

/**
 * This is going to act as a test for
 * {@link it.unibo.oop.lab.enum1.SportSocialNetworkUserImpl}
 * 
 * 1) Realize the same test as the previous exercise, this time using
 * enumeration Sport
 * 
 * 2) Run it: every test must return true.
 * 
 */
public final class TestSportByEnumeration {

    private TestSportByEnumeration() {
    }

    /**
     * @param args
     *            ignored
     */
    public static void main(final String... args) {
        List<User> l = new ArrayList<>();
        SportSocialNetworkUserImpl<User> u1 = new SportSocialNetworkUserImpl<>("Francesco", "Amadori", "francesco.a97", 19);
        SportSocialNetworkUserImpl<User> u2 = new SportSocialNetworkUserImpl<>("Nicholas", "Bertozzi", "bertozpiccio", 19);
        SportSocialNetworkUserImpl<User> u3 = new SportSocialNetworkUserImpl<>("Elisa", "Curatola", "disastro1998", 19);
    	l.add(u1);
    	l.add(u2);
    	l.add(u3);
    	u1.addSport(Sport.SOCCER);
    	u1.addSport(Sport.BASKET);
    	u2.addSport(Sport.BIKE);
    	u2.addSport(Sport.BIKE);
    	u3.addSport(Sport.VOLLEY);
    	System.out.println(u1.hasSport(Sport.SOCCER));
    	System.out.println(u2.hasSport(Sport.BIKE));
    	System.out.println(u3.hasSport(Sport.VOLLEY));
    	System.out.println(u1.hasSport(Sport.BASKET));
    	System.out.println(u1.hasSport(Sport.VOLLEY));
        /*
         * [TEST DEFINITION]
         * 
         * By now, you know how to do it
         */
        // TODO
    }

}
